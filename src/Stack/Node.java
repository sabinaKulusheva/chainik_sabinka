package Stack;

public class Node {
    int data;
    Node nextNode;

    public Node(data) {
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Stack.Node getNextNode() {
        return nextNode;
    }

    public void setNextNode(Stack.Node nextNode) {
        this.nextNode = nextNode;
    }
}
