package week4Stack;

public class Stack {
    Node top;

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }

    public void pop() {
        Node popped;
        if (top == null) {
            System.out.println("Stack is empty");
        } else {
            popped = top;
            top = top.nextNode;
        }
    }

    public void push(int data) {
        Node pushed = new Node(data);
        top = pushed;
    }

    public int size(){
        int counter = 0;
        if(top != null) counter++;
        top = top.nextNode;
        return counter;
    }

    public void stackIsEmpty(){
        if(top == null) System.out.println("Stack is empty");
    }

    public void top(int data){
        Node pushed = new Node(data);
        System.out.println(data);
    }
}
