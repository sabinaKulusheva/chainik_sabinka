package HW;
import java.util.Scanner;
public class Max {
    public static  void main(String[] args) {
        System.out.println(maxNum());
    }
    public static  int maxNum(){
        Scanner input = new Scanner(System.in);
        int num=input.nextInt();
        if(num==0) {
            return 0;
        }
        int max=maxNum();
        if(max>=num){
            return num;
        }
        return num;
    }
}

