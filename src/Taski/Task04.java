package Taski;
import java.util.Scanner;
public class Task04 {
    private static void ar(int n,int a[][], int k,int p){
        if(k>n*n){
            return;
        }
        for(int i=p;i<n-p;i++){
            a[p][i]=k;
            k=k+1;
        }
        for (int i=p+1;i<n-p;i++){
            a[i][n-1-p]=k;
            k=k+1;
        }
        for(int i=n-p-2;i>=p;i--){
            a[n-p-1][i]=k;
            k=k+1;
        }
        for(int i=n-p-2;i>p;i--){
            a[i][p]=k;
            k=k+1;
        }
        ar(n, a, k,p+1);
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int [][] a = new int[n][n];
        ar(n, a, 1, 0);
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                System.out.print(a[i][j]+" ");
            }
            System.out.print("\n");
        }
    }
}

