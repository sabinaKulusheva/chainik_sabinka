package Taski;
import java.util.Scanner;
public class Task02 {


    private static void seq(int n){
        Scanner sc = new Scanner(System.in);
        if(n>0){
            int x=sc.nextInt();
            seq(n-1);
            System.out.println(x);
        }
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        seq(n);
    }
}

