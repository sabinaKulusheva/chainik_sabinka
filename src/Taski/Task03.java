package Taski;
import java.util.Scanner;
public class Task03 {
    private static void seq(int n){
        Scanner sc = new Scanner(System.in);
        if(n>0){
            String x=sc.next();
            seq(n-1);
            System.out.println(x);
        }
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        seq(n);
    }
}
