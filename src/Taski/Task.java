package Taski;
import java.util.Scanner;
public class Task {
    private static boolean maze_runner(char[][] arr){
        return maze_runner(0, 0, arr, false);
    }
    private static boolean maze_runner(int i, int j, char[][] arr, boolean is_find){
        is_find = false;
        if (i==arr.length-1 && j==arr[0].length-1) is_find = true;
        if(j+1<arr[0].length && arr[i][j+1]!='#') return is_find||maze_runner(i,j+1,arr,is_find);
        if (i+1<arr.length && arr[i+1][j]!='#') return is_find||maze_runner(i+1,j,arr,is_find);
        if (j-1<arr[0].length && arr[i][j-1]!='#') return is_find||maze_runner(i,j-1,arr,is_find);
        if (i-1>arr.length && arr[i-1][j]!='#') return is_find||maze_runner(i-1,j,arr, is_find);
        return is_find;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        char[][] arr = new char[n][m];
        for (int i=0;i<n;i++)
            for (int j=0;j<m;j++) arr[i][j]=sc.next().charAt(0);
        if(maze_runner(arr)) System.out.println("YES"); else System.out.println("NO");
    }
}

